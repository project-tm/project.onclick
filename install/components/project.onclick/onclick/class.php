<?php

use Bitrix\Main\Application,
    Bitrix\Main\Loader;

class projectOnclick extends CBitrixComponent {

    public function executeComponent() {
        $this->arResult['CAPTCHA'] = false;
        $this->arResult['ERROR'] = '';
        $request = Application::getInstance()->getContext()->getRequest();
        if (Loader::includeModule('project.onclick')) {
            if ($request->get('isSend')) {
                try {
                    $this->arResult['ORDER_ID'] = $this->actionOrder($request);
                } catch (Exception $exc) {
                    $this->arResult['ERROR'] = $exc->GetMessage();
                }
            } elseif (cUser::IsAuthorized()) {
                $this->arResult['ORDER'] = array(
                    'NAME' => cUser::GetFullName(),
                    'PHONE' => cUser::GetParam('PERSONAL_PHONE'),
                    'EMAIL' => cUser::GetEmail(),
                );
            }
        } elseif ($_POST) {
            $this->arResult['ERROR'] = 'Ошибка сервера, повторите запрос позднее';
        }

        $this->arResult["SECURE_AUTH"] = false;
        if (!CMain::IsHTTPS() && COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y') {
            $sec = new CRsaSecurity();
            if (($arKeys = $sec->LoadKeys())) {
                $sec->SetKeys($arKeys);
                $sec->AddToForm('system_auth_form' . $arResult["RND"], array('USER_PASSWORD'));
                $this->arResult["SECURE_AUTH"] = true;
            }
        }

        if ($this->arResult['CAPTCHA']) {
            global $APPLICATION;
            $this->arResult["CAPTCHA_CODE"] = $APPLICATION->CaptchaGetCode();
        }

        $this->includeComponentTemplate();
    }

    public function initCaptcha($captcha) {
        $this->arResult['CAPTCHA'] = (bool) $captcha;
//        pre($this->arResult['CAPTCHA']);
    }

    private function actionOrder($request) {
        $this->arResult['ORDER'] = array(
            'NAME' => trim($request->get('NAME')),
            'PHONE' => trim($request->get('PHONE')),
            'EMAIL' => trim($request->get('EMAIL')),
            'COMMENT' => trim($request->get('COMMENT')),
        );
        if (empty($this->arResult['ORDER']['NAME'])) {
            throw new Exception('Необходимо указать имя');
        }
        if (empty($this->arResult['ORDER']['PHONE'])) {
            throw new Exception('Необходимо указать телефон');
        }
        return Project\Onclick\Order::create($this->arResult['ORDER']);
    }

}
