<? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    <div class="commerce-buy-one-click-form tHidden <?= $arParams['WRAPPER']["CONTANER"] ?>">
    <? } ?>
    <form action="javascript:console.log(1);" id="commerce-buy-one-click-form" class="<?= $arParams['WRAPPER']["CONTANER_FORM"] ?>" data-custom-submit="1">
        <div>
            <div id="popup-info-box"></div>
            <? if (empty($arResult['ORDER_ID'])) { ?>
                <h1 class="title">Купить в один клик</h1>
                <? if (!empty($arResult['ERROR'])) { ?>
                    <div class="form-item form-type-textfield form-item-commerce-buy-one-click-name">
                        <?= $arResult['ERROR'] ?>
                    </div>
                <? } ?>
                <div class="form-item form-type-textfield form-item-commerce-buy-one-click-name">
                    <input required="required" placeholder="ИМЯ" type="text" id="NAME" name="NAME" value="<?= htmlspecialcharsbx($arResult['ORDER']['NAME'] ?: '') ?>" size="60" maxlength="128" class="form-text" />
                </div>
                <div class="form-item form-type-textfield form-item-commerce-buy-one-click-phone">
                    <input required="required" placeholder="ТЕЛЕФОН" type="text" id="PHONE" name="PHONE" value="<?= htmlspecialcharsbx($arResult['ORDER']['PHONE'] ?: '') ?>" size="60" maxlength="128" class="form-text" />
                </div>
                <div class="form-item form-type-textfield form-item-commerce-buy-one-click-email">
                    <input placeholder="E-MAIL" type="email" id="EMAIL" name="EMAIL" value="<?= htmlspecialcharsbx($arResult['ORDER']['EMAIL'] ?: '') ?>" size="60" maxlength="128" class="form-text required" />
                </div>
                <div class="form-item form-type-textarea form-item-commerce-buy-one-click-comment">
                    <div class="form-textarea-wrapper resizable">
                        <textarea placeholder="КОММЕНТАРИЙ" id="COMMENT" name="COMMENT" cols="60" rows="5" class="form-textarea"><?= htmlspecialcharsbx($arResult['ORDER']['COMMENT'] ?: '') ?></textarea>
                    </div>
                </div>
                <? if ($arResult["SECURE_AUTH"]): ?>
                    <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
                        <div class="bx-auth-secure-icon"></div>
                    </span>
                    <noscript>
                    <span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
                        <div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
                    </span>
                    </noscript>
                    <script type="text/javascript">
                        document.getElementById('bx_auth_secure<?= $arResult["RND"] ?>').style.display = 'inline-block';
                    </script>
                <? endif ?>
                <? if ($arResult["CAPTCHA_CODE"]): ?>
                    <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>" />
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" class="captcha_sid" />
                    <input type="text" name="captcha_word" maxlength="50" value="" placeholder="Введите код с картинки" required/>
                <? endif ?>
                <input type="hidden" name="isSend" value="1"/>
                <input type="button" id="edit-cancel" name="op" value="Отмена" class="form-submit" />
                <input class="use-ajax-submit form-submit" type="submit" id="edit-submit" name="op" value="Заказать" />
            <? } else { ?>
                <h1 class="title">Спасибо за заказ</h1>
                <p>Наши менеджеры свяжутся с Вами</p>
            <? } ?>
        </div>
    </form>
    <script>
        var oneClickWrapper = <?= $arParams['WRAPPER']['JS_OBJECT'] ?>;
        oneClickWrapper.stop = function () {
            $.fancybox.update();
        };
    </script>
    <? if (empty($arParams['WRAPPER']['IS_AJAX'])) { ?>
    </div>
<? } ?>