<?php

use Bitrix\Main\Loader;

Loader::includeModule('project.core');
Loader::includeModule('iblock');
Loader::includeModule('catalog');
Loader::includeModule('sale');
