# Модуль заказа в 1 клик #

- данная версия создает заказ (то что есть в корзине)
- при клике отправить, дергается стандартный запрос, с детальной странице, добавить в корзину

инициализация
```php
$APPLICATION->IncludeComponent("project:ajax.wrapper", "onclick");
```

создается кнопка для вызова popup
```html
<span class="commerce-buy-one-click-button form-submit"></span>
```